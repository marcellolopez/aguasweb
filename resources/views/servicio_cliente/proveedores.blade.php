@extends('layouts.m3')

@section('title', 'Proveedores')
@section('servicio-cliente', 'active')
@section('proveedores', 'active')

@section('sidebar')
@endsection


@section('content')



<div class="content">
  <h2><span>Proveedores</span></h2>

<p>Informacion Proveedores</p>
<div><strong>CLAUSULAS ORDEN  DE COMPRA</strong></p>
<h3><strong>A.- CALIDAD</strong></h3>
<p><strong> </strong></p>
<p>1. Los materiales y/o equipos  entregados deberán cumplir con las normas y/o especificaciones técnicas  estipuladas.</p>
<p>2. AGUASDECIMA S.A., se reserva el  derecho de inspeccionar la  calidad de los materiales empleados y de supervisar  la totalidad o  parte del proceso de fabricación de los elementos que son  adquiridos  con la   presente Orden de Compra, para lo cual el proveedor deberá   otorgar el máximo de facilidades.  En  todo caso, esta inspección no  libera al proveedor de su total y exclusiva  responsabilidad de la  correcta fabricación de los mismos.</p>
<p>3. AGUASDECIMA S.A., se reserva el  derecho de delegar la  inspección durante la fabricación y/o las pruebas y  ensayos de  recepción en un laboratorio competente a juicio de AGUASDECIMA S.A.</p>
<p>4. El proveedor otorga una garantía  mínima de 12 meses a contar de la fecha de recepción de obras, MATERIALES Y/O  EQUIPOS.</p>
<p><strong> </strong></p>
<h3><strong>B.- PRECIOS</strong></h3>
<p><strong> </strong>5. Los precios indicados son fijos,  salvo que se estipule expresamente una cláusula de reajuste.</p>
<p>6. En caso de estipularse una  cláusula de reajustes, ésta regirá  sólo hasta la fecha de entrega estipulada en  la Orden de Compra.  En  caso de entregas  con posterioridad a la fecha convenida, el precio  se  mantendrá fijo a contar de dicha fecha,  sin perjuicio de la aplicación  de las multas estipuladas.</p>
<p>7. En caso de atraso en la entrega,  salvo fuerza mayor, se  aplicará una multa de 0,2% diario; hasta el 10° día; 0,3%  diario entre  el día 11 y el 20° día; 0,4% diario entre el 21° y 30° día; 0,5%  diario  desde el 31° día en adelante.  Tope  máximo de la multa . 20%.</p>
<p>8. En caso que la fecha estipulada  para una entrega sea día  Sábado, Domingo o festivo, la entrega deberá  verificarse el primer día  hábil siguiente.</p>
<p>9. Salvo se estipule expresamente en  contrario, los precios  serán por los   materiales yo equipos puestos en nuestro Almacén,  ubicado en calle  Picarte N° 1875 esquina Huemul, Valdivia.</p>
<p>10. El precio incluye embalaje.</p>
<p><strong> </strong></p>
<h3><strong>C.FORMA DE PAGO</strong></h3>
<p><strong> </strong>11. El proveedor deberá enviar  factura en triplicado y  documentación de despacho a nuestra Oficina, ubicada en  Calle Arauco N°  434, Valdivia.</p>
<p>12. En la factura y documentación de  despacho se deberá indicar el número de la Orden de compra.</p>
<p>13. Salvo que se estipule  expresamente una forma de pago  diferente, AGUASDECIMA S.A., pagará entre 30 o  45 días desde la fecha  de recibida la   Factura, Boleta de Compra Venta y/o de Servicio de  nuestro  proveedor.  Para lo cual se debe tener en  cuenta que nuestras  fechas de pago son los días 09 y 27 de cada mes, o el día  hábil  inmediatamente siguiente.</p>
<p>14. El pago de la(s) factura(s) se  efectuará por medio de Vale  Vista en la Oficina del Banco Santander que esté  más próxima al  domicilio de nuestro(s) proveedor(es).</p>
<p>15. En Caso que el proveedor desee  se le remita el cheque  correspondiente al pago de la factura por correo  certificado, debe  enviar la factura debidamente cancelada.  En tal caso, el cheque será  despachado a la  fecha de vencimiento, según cláusula 13.</p>
<p>16. En caso que en la(s) factura(s)  presentada(s) por el  proveedor se indiquen condiciones de pago distintas de las  estipuladas  en la Orden de Compra, esas condiciones se tendrá por no escritas.</p>
<p>17. El proveedor podrá ofrecer un  descuento por pago con  anterioridad a la fecha estipulada en la orden de  compra, para lo cual  deberá indicarlo en la Factura.  AGUASDECIMA S.A.,  se reserva el  derecho de aceptar o rechazar el descuento ofrecido.</p>
<p>18. En caso que el proveedor deba  emitir Nota de Crédito, ésta  deberá ser recibida siete días antes de la fecha  de pago de la factura  correspondiente.   En caso contrario, el pago de la factura se efectuará  a siete días de  recibida la Nota de Crédito.</p>
<p><strong> </strong></p>
<h3><strong>D. GENERALES</strong></h3>
<p><strong> </strong>19. No serán consideradas como  atraso en la entrega, situaciones de  fuerza mayor que afecten al proveedor que  hayan sido comunicadas con  los antecedentes que lo acrediten tan pronto se  produzca tal  situación.  La comunicación  de la fuerza mayor deberá efectuarse con  anterioridad a la fecha de entrega  estipulada. AGUASDECIMA S.A.,  calificará los antecedentes presentados y  determinará, a su solo  juicio, la validez de la fuerza mayor.</p>
<p>20. Las entregas de materiales yo equipos  sólo se considerarán  completas si se han cumplido todas las condiciones  estipuladas en la    presente Orden de Compra.</p>
<p>21. AGUASDECIMA S.A., se reserva el  derecho de revocar la orden  de Compra en cualquier momento, por incumplimiento  del proveedor en los  plazos de entrega y/o de la calidad de las especies  ordenadas.</p>
<p>22. AGUASDECIMA S.A., se reserva el  derecho de cobrar daños y  perjuicios, además de las multas estipuladas, que  ocasione el  incumplimiento de los plazos de entrega y/o de la calidad  especificada.</p>
<p>23. El proveedor indemnizará a  AGUASDECIMA S.A., contra  perjuicio a cuenta de patentes de invención, etc., que  se refieran con  el material y/o equipo cubierto por la Orden de compra.</p>
<p>24. El proveedor debe enviar copia  Control Interno debidamente  firmada en señal de aceptación, de lo contrario no  se procederá al pago  de factura.</p>
<p>25. AGUAS DECIMA S.A. se reserva el  derecho de solicitar Boletas  de Garantía para cautelar cumplimiento de  contratos por parte de los  proveedores de bienes ó servicios.</p>
</div>

@endsection