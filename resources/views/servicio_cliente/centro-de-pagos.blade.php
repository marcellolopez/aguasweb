@extends('layouts.m3')

@section('title', 'Centro de pagos')
@section('servicio-cliente', 'active')
@section('centro-de-pagos', 'active')

@section('sidebar')
@endsection


@section('content')



<div class="content">
<h2><span>Centros de Pagos</span></h2>
<div class="col-md-6">
<p><strong>Oficina Comercial </strong></p>
<ul type="disc">
<li><span>Arauco 434 Valdivia</span></li>
</ul>
<p style="text-align: center;"><span> </span></p>
<p><span><strong>Otros Centros de Pago</strong></span></p>
<ul type="disc">
<li><span>Servipag, Arauco N° 561 (Mall Plaza de los Rios)</span></li>
<li><span>Servipag, Supermercado Santa Isabel, Chacabuco N° 555</span></li>
<li><span>Servipag, Supermercado Lider, Bueras N° 1400</span></li>
<li><span>Servipag, Arauco N° 331</span></li>
<li><span>Caja Vecina, Poblacion San Pablo, minimarket la golondrina</span></li>
<li><span><span>Pedro Aguirre Cerda N° 244, Local Comercial Compuquen &#8211; Valdivia</span></span></li>
</ul>
</div>
<div class="col-md-6">
<p style="text-align: center;"><span><a href="https://www.aguasdecima.cl/wp-content/uploads/2010/10/cajas.jpg"><img class="aligncenter size-full wp-image-470" title="cajas" src="https://www.aguasdecima.cl/wp-content/uploads/2010/10/cajas.jpg" alt="" width="363" height="195" /></a></span></p>
</div>
</div>

    @endsection