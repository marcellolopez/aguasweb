@extends('layouts.m3')

@section('title', 'Reglamentación y Compromiso')
@section('servicio-cliente', 'active')
@section('reglamentacion-y-compromiso', 'active')

@section('sidebar')
@endsection


@section('content')



<div class="content">
<h2><span>Reglamentación y Compromiso</span></h2>

<p>Principales leyes y reglamentos del sector  sanitario:</p>
<div>
<p>D.F.L. N° 382   de 1989 “Ley General de Servicios Sanitarios”<br />
Decreto N° 1.199   de 2005“ Reglamento de la Ley General  de Servicios Sanitarios”</p>
<p>D.F.L.  N°  70 de 1988  “Ley de Tarifas de Servicios  Sanitarios “<br />
Decreto N° 453 de 1989 “Reglamento de la Ley de Tarifas “</p>
<p>D.S- N° 50 de 2003 “Reglamento de Instalaciones  Domiciliarias de Agua Potable y Alcantarillado “</p>
<p>Ley N° 18.119, Artículo 459 “Sanciones por  Servicios Fraudulentos”<br />
Ley N° 18.902 de 1990 “Crea la Superintendencia  de Servicios Sanitarios “.</p>
<p>¿Qué se entiende por cliente de los servicios sanitarios?</p>
<p>Se entiende por usuarios o cliente de un servicio publico de  distribución de agua potable o de recolección de aguas servidas, a la  persona natural o jurídica que habite o resida en el inmueble que recibe  el servicio.</p>
<p>¿Cuáles son los derechos del cliente?</p>
<p>•  Recibir un servicio continuo y de calidad. Agua potable de calidad, en cantidad y presión suficientes, según necesidades.<br />
•  Ser atendido por la empresa oportunamente en cuanto a sus reclamos.<br />
•  Que se le otorgue la factibilidad requerida en el área de servicio sin sujeción a condiciones especiales, salvo aquellas consideradas en la normativa, constitución de servidumbres,<br />
plazo de ejecución de las obras.<br />
•  Solicitar a la empresa, verificar la exacta medición del medidor en uso, en base a los procedimientos que determine la superintendencia.<br />
•  Que se carguen a su cuenta los valores de corte y reposición del servicio, cuando estos hayan sido efectivamente ejecutados por el prestador.<br />
•  Presentar directamente ante la superintendencia reclamaciones relacionadas con la calidad de atención de los usuarios y prestación de los servicios otorgados por las<br />
concesionarias.</p>
<p>¿Cuáles son las obligaciones del cliente?</p>
<p>•  Pagar las sumas adeudadas, dentro del plazo establecido en la respectiva boleta o factura.<br />
•  Requerir la boleta o factura pendiente en las oficinas de la empresa, cuando no la hubiere recibido dentro del plazo correspondiente.<br />
•  Verificar los valores contenidos en las boletas o facturas, entendiéndose que otorga su conformidad si no presentare reclamo escrito ante la empresa dentro del plazo de 60 días<br />
contados desde el vencimiento del plazo para el pago.<br />
•  Usar correctamente las instalaciones domiciliarias y no vaciar en los desagües de alcantarillado, objetos, basuras o materias sólidas.</p>
<p><em>Fuente de la información desde <a href="http://www.siss.cl/shnoti.asp?noticia=1302" target="_blank">Superintendencia de Servicios Sanitarios</a><br />
</em></p>
<div>Deberes y obligaciones: áreas de acción</div>
<p>Limites en atención en agua potable y alcantarillado:</p>
<p>Aguasdecima atiende la mantención y reparación de las instalaciones hasta estos límites; el resto corresponde al cliente.</p>
<p>Agua Potable:<br />
<strong>Límite Aguasdécima</strong> Hasta llave de paso interior, inclusive.</p>
<p>Alcantarillado<br />
<strong>Límite Aguasdécima</strong> Hasta el inicio de la unión domiciliaria en la salida de la cámara de inspección.</p>


</p>
           
</div>

    @endsection