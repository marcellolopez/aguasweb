@extends('layouts.m3')

@section('title', 'Preguntas Frecuentes')
@section('servicio-cliente', 'active')
@section('preguntas-frecuentes', 'active')

@section('sidebar')
@endsection


@section('content')



<div class="content">
<h2><span>Preguntas Frecuentes</span></h2>

<p ><strong><span>¿Cuál es la fecha límite de mi cuenta de agua? </span></strong></p>
<p >Los cargos actuales vencen 25 días después de  impresa su boleta, y  si al generarse una nueva boleta habiendo aún cargos  pendientes, corre  el riesgo de una interrupción de su servicio.</p>
<p ><strong><span>¿Cómo solicito un servicio de emergencia? </span></strong></p>
<p >Para pedir que interrumpan el suministro de agua de inmediato o para  reportar la ruptura de un medidor de agua, una tubería, una bomba de  agua o bien una cañería de aguas residuales tapada o que se haya  desbordado, llame al fono 214423.</p>
<p ><strong><span>¿Cómo establezco un nuevo servicio de  suministro y recolección de aguas en una propiedad inmobiliaria que aún  no está conectada al sistema? </span></strong></p>
<p >Comuníquese con la unidad de distribución ubicado en Huemul o al teléfono 214423.</p>
<p ><strong><span>¿Por qué es el monto de mi factura tan alto? </span></strong></p>
<p >Su factura se basa en la cantidad de agua  registrada por su  medidor, bien sea que la use o la derroche, usted paga por  cada gota.   Es por eso que es importante usar el agua concientemente.</p>
<p ><span><strong>¿Por qué  en el valor de mi factura el alcantarillado es más alto que el consumo de agua potable? </strong></span></p>
<p >La superintendencia de Servicios Sanitarios exige que se traten las  aguas residuales antes de regresarlas al medio ambiente.  Cuanto más  sucia esté el agua, más caro resulta sanearla.  Las aguas residuales  están más sucias que el agua que extraemos de nuestras captaciones para  luego consumirla.</p>
<p ><strong><span>¿Cómo se calcula la cuota de recolección de aguas servidas de una factura residencial? </span></strong></p>
<p >Ya que no es eficiente desde el punto de vista  económico medir las  aguas residuales, la Superintendencia  de Servicios Sanitarios hace un  cálculo estimado del costo de  recolección.  Para hacer este cálculo se  usa el promedio de las facturas  de los meses de invierno.  Se utilizan  estos cuatro meses porque es cuando  los usuarios usan menos agua para  el riego.  No obstante, si un cliente  usa menos agua en el mes actual  que en el promedio de los meses de  invierno, la cuota de aguas  residuales se basa en el consumo de agua del mes actual.<br />
<strong><br />
<span>¿Qué causa los cambios de olor y sabor ocasionales del agua? </span></strong></p>
<p >El agua siempre tiene características de sabor y olor propias.  El  sistema de suministro de agua experimenta ocasionalmente cambios en el  olor y el sabor del agua.  En el Verano y en la primera parte del Otoño,  las algas le dan al agua un sabor y olor a tierra peculiar.  Asimismo,  los cambios de temperatura y las precipitaciones excesivas pueden  alterar el sabor del agua.  Estos cambios no afectan la inocuidad del  agua potable.</p>
</div>

@endsection