@extends('layouts.m3')

@section('title', 'Centro de atencion al cliente')
@section('servicio-cliente', 'active')
@section('centro-de-atencion-al-cliente', 'active')

@section('sidebar')
@endsection


@section('content')



<div class="content">
  <h2><span>Centro de Atención Clientes</span></h2>
                    

<div class="col-md-6" ><strong><span >Gerencia General</span>
</strong><a href="https://www.aguasdecima.cl/wp-content/uploads/2010/10/gerencia21.jpg"><img class="alignright size-full wp-image-473" title="gerencia2" alt="" src="https://www.aguasdecima.cl/wp-content/uploads/2010/10/gerencia21.jpg" width="232" height="177" /></a>
<br />
Dirección<br />
Arauco 434 Valdivia<br />
Fono 63 2213321<br />
Fax 63 2213212 <strong><br />
Horario Atención de Público<br />
</strong>Lunes a Viernes mañana 08:30 a 13:00 hrs.<br />
tarde 14:30 a 18:00 hrs.&nbsp;</p>
</div>
<div class="col-md-6"><strong><span >Oficina Comercial</span>
</strong><a href="https://www.aguasdecima.cl/wp-content/uploads/2010/10/cliente1.jpg"><img class="alignright size-full wp-image-474" title="cliente" alt="" src="https://www.aguasdecima.cl/wp-content/uploads/2010/10/cliente1.jpg" width="231" height="175" /></a>
<br />
Dirección<br />
Arauco 434 Valdivia<br />
Fono 63 2211109 · 63 2213322<strong><br />
Horario Atención de Público<br />
</strong>Lunes a Viernes<br />
mañana 08:30 a 13:00 hrs.<br />
tarde 14:30 a 18:00 hrs</div>
<div class="col-md-12"><strong><span >Oficina Técnica</span><br />
</strong>Dirección<br />
Huemul 519 Valdivia<br />
Fono 63 2207753<br />
<strong>Horario Atención de Público</strong><br />
Lunes a Viernes<br />
mañana 08:30 a 13:00 hrs.<br />
tarde 14:30 a 18:00 hrs.</div>
           
</div>

    @endsection