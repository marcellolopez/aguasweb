@extends('layouts.m3')

@section('title', 'Subsidio')
@section('servicio-cliente', 'active')
@section('subsidio', 'active')

@section('sidebar')
@endsection


@section('content')



<div class="content">
<h2><span>Subsidio</span></h2>
<p>Subsidio al consumo de Agua Potable y Servicio de Alcantarillado  (Ley 18.778 de 1989)<br />
Este beneficio consiste en que el Estado cubre parte del consumo de   Agua Potable y Servicio de Alcantarillado a través de los Municipios, y   está destinado a las familias de escasos recursos económicos.<br />
Para la ciudad de Valdivia, el porcentaje que cubre éste beneficio es   del 65% del total del Cargo Fijo y de los cargos variables, con un tope   de 15 metros cúbicos.<br />
Para optar a este beneficio, las familias deben inscribirse en el   Departamento Social del Municipio y encontrarse al día en los pagos de   sus servicios.<br />
Este subsidio tiene una duración de tres años, el cual puede ser   renovado por las familias beneficiadas en el mismo Departamento Social.</p>
</div>

@endsection