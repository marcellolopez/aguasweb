@extends('layouts.m2')

@section('title', 'Plantas de agua potable')
@section('empresa', 'active')
@section('plantas-de-agua-potable', 'active')

@section('sidebar')
@endsection


@section('content')



<div class="content">
 <h1><span>Plantas Agua Potable</span></h1>
 <h4><span><strong>Planta de Tratamiento de Llancahue</strong></span></h4>
 <div class="row-fluid">
  <div class=""> 
   <p><span>Esta Planta ubicada a la salida sur de la ciudad, se abastece de  agua desde el estero &#8220;Llancahue&#8221;, ubicado en una cuenca hidrográfica  protegida. El origen del agua es totalmente pluvial, por lo que en época  de estiaje su capacidad no permite satisfacer la demanda de agua de la  población. </span></p>
  </div>
    <div class="col-md-5">
   <p><img  title="cap-llancahue" src="https://www.aguasdecima.cl/wp-content/uploads/2010/10/cap-llancahue1.jpg" alt="" width="287" height="213" /></p>
  </div>
 </div>
 <br>
 <p><span><strong>Captación</strong>:</span></p>
 <p><span>En la captación del agua existe una barrera frontal que hace las  veces de un pequeño embalse, desde donde se conduce el agua hasta la  Planta de Tratamiento a través de una cañería de 700 mm de diámetro, que  posteriormente se convierte en dos tuberías de 300 y 400 mm.</span></p>
 <p><span><strong>Potabilizacion:</strong></span></p>
 <p><span>En la planta de tratamiento el agua pasa por una canaleta, lugar  donde se mezclan los productos químicos: cal, cloro y sulfato de  aluminio.<br />
  Siguiendo su camino, el agua llega hasta un estanque floculador  (Volumen 272 m3 ), aquí se produce la unión y aglomeración de las  partículas coloidales causantes de la turbidez, llamadas flóculos. Este  efecto es favorecido por la turbulencia generada por dos revolvedores  electromecánicos. La mezcla formada en la fase anterior llega hasta dos  unidades sedimentadoras (Volumen total de 2.607 m3 ). En esta etapa, los  flóculos producidos son removidos del agua por la gravedad debido a su  mayor peso.<br />
  Sin embargo, el agua aún presenta algunas partículas y flóculos  livianos que no lograron sedimentar, por lo que es imprescindible una  etapa de filtración para su retención y eliminación posterior.<br />
  El sistema de filtración está compuesto por 6 unidades independientes.  Presentan como material filtrante un lecho mixto formado por una capa  superior de carbón ( 45 cm . de espesor), y otra de arena cuarcítica de  25 cm . ambas soportadas por un lecho de grava de 30 cm.</span><br />
  <img src="https://www.aguasdecima.cl/wp-content/uploads/2010/10/diag-llancahue.jpg" alt="Diagrama Llancahue" width="470" height="308" /><br />
  <span>Con el propósito de reducir y eliminar la presencia de microorganismos  potencialmente peligrosos para la salud humana, se realiza el proceso  de desinfección. El agente desinfectante usado es una solución de cloro.  Posteriormente, el agua se conduce hasta un estanque de almacenamiento  de 780 m (3), donde además se le aplica una dosis de flúor.</span></p>
  <h3><span><strong>Planta de Tratamiento Cuesta de Soto</strong></span></h3>
  <p><span><strong>Captación</strong>:</span></p>
  <p><img src="https://www.aguasdecima.cl/wp-content/uploads/2010/10/cap_soto.jpg" alt="" width="199" height="255" align="right" /><span>Este  sistema capta el agua desde la ribera sur del río Calle Calle,  aproximadamente a unos 8 Km . al oriente de la ciudad, mediante una  cañería de acero de 700 mm de diámetro que se comunica con una sentina.  Desde aquí el agua es aspirada e impulsada mediante 4 equipos motobombas  de 300 HP cada uno, hasta dos estanques de 500 m3. Estos se ubican el  la cima del cerro Cuesta Soto que están diseñados para prevenir el golpe  de ariete y alimentar la planta de tratamiento, respectivamente.</span></p>
  <p><span><strong>Potabilizacion:</strong></span></p>
  <p><span>El agua ingresa a la planta a través de una canaleta de hormigón  armado, diseñada para producir la mezcla del agua con los productos  químicos necesarios para el proceso. En esta planta se agrega cal y  sulfato de aluminio en función de las características del agua cruda.</span></p>
  <p><span>La canaleta también tiene como misión determinar el caudal y la turbidez del agua de entrada.</span><br />
    <span>Una vez que el agua ha entrado en contacto con los productos  químicos es conducida hasta el sistema de filtración. Previo a ello, el  agua recibe una dosis de cloro (precloración), con el objeto de eliminar  organismos molestos, en especial las algas.</span></p>
    <p><span>Los filtros están constituidos por 8 unidades con características de  filtración con tasa declinante. El material filtrante está formado por  un lecho mixto que consta de una capa superior de 45 cm . de carbón y  otra de arena cuarcítica de 25 cm ., las que se apoyan sobre un lecho de  grava de 30 cm.</span></p>
    <p><span>El agua filtrada se desinfecta con una solución de cloro para  garantizar su calidad sanitaria, siendo entonces conducida hasta un  estanque de hormigón armado de 300 m3 de capacidad. Desde éste, a través  de una cañería de 800 mm de diámetro, se traslada por la gravedad hasta  el Estanque Picarte y desde allí a la población para su consumo.</span><br />
      <img src="https://www.aguasdecima.cl/wp-content/uploads/2010/10/diag_soto.jpg" alt="" width="470" height="313" /><br />
      <span>Finalmente, tanto desde la Planta Llancahue , como de  Cuesta de  Soto, el agua es enviada por gravedad mediante sendas aducciones de   diámetros 400,  500,  630 y 800   mm., hasta los Estanques de  Regulación   y Reserva, con capacidad para almacenar 10.000 m3 ubicados  en  distintos puntos de la ciudad, en los sectores Inés de Suárez y  Avda. Picarte,  esquina Huemul,  y luego desde allí se  distribuye a la  población para su consumo.</span></p>
    </div>

    @endsection