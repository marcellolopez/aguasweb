@extends('layouts.m2')

@section('title', 'Reseña Historica')
@section('empresa', 'active')
@section('datos-de-empresa', 'active')

@section('sidebar')
@endsection


@section('content')



  <div class="content">
    <h1><span>Datos Empresa</span></h1>

    <p><strong>IDENTIFICACIÓN DE LA EMPRESA</strong></p>
    <p>Nombre : Aguas Décima S.A.<br />
      Tipo de Sociedad: Sociedad Anónima Cerrada<br />
      Rut: 96.703.230-1<br />
      Domicilio Legal: Avda. Andres Bello 2687, piso 22 Las Condes, Santiago.<br />
      Domicilio Comercial: Arauco 434, Valdivia.<br />
      Telefonos: (63) 2213321<br />
      Fax: (63) 2213212<br />
      E-mail: : <a href="mailto:info@aguasdecima.cl">info@aguasdecima.cl</a></p>
      <h3><strong>Organización</strong></h3>
      <p>Presidente Directorio<br />
        Toshimitsu Oda</p>
        <p>Gerente General<br />
          Eduardo Vyhmeister Hechenleitner &#8211; Constructor Civil</p>
          <p>Jefe Departamento de Operaciones<br />
            Ricardo Rosales Rosales &#8211; Ingeniero Constructor</p>
            <p>Asesor de Calidad<br />
              Gerardo Marcuello Aguierre &#8211; Químico</p>
              <p>Jefe Departamento de Administración General<br />
                Carlos Urbina Miranda &#8211; Contador Auditor</p>
                <p>Jefe Departamento de Planificación y Control<br />
                  Alex Gutiérrez González &#8211; Ingeniero Constructor</p>
                  <p><strong>Organigrama</strong></p>
                  <p style="text-align: center;"><strong><a href="https://www.aguasdecima.cl/wp-content/uploads/2010/10/Organigrama1.jpg"><img class="aligncenter size-full wp-image-429" title="Organigrama" alt="" src="https://www.aguasdecima.cl/wp-content/uploads/2010/10/Organigrama1.jpg" width="433" height="750" /></a></strong></p>
                  <p style="text-align: justify;"><strong>Funcionarios</strong></p>
                  <p style="text-align: justify;">La dotación total de personal de la empresa es de 66 trabajadores.<br />
                    El ambiente laboral es bueno, la comunicación entre la administración de la empresa y sus trabajadores es fluida, transparente y armónica. Es asi que el 29 de Septiembre de 2009 se suscribio un nuevo Contrato Colectivo de Trabajo, que rige por el periodo Octubre de 2009 hasta el 30 de Septiembre de 2012, entre el Sindicato de Trabajadores y la Empresa. Las buenas relaciones dadas entre ambas partes, han permitido la integración de los trabajadores de todas las áreas de la empresa a los comités de “Planificación Estratégica”, participando en proponer el horizonte de nuestra empresa para los próximos diez años.</p>
 </div>

@endsection