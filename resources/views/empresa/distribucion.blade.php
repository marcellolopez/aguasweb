@extends('layouts.m2')

@section('title', 'Distribución')
@section('empresa', 'active')
@section('distribucion', 'active')

@section('sidebar')
@endsection


@section('content')



<div class="content">
<h2><span>Redes de Distribución</span></h2>
<div style="text-align: justify;">
<p><span>Para el cumplimiento de sus planes comerciales y de su rol de  servicio al cliente, la empresa cuenta con la infraestructura de  Producción, Redes de Distribución de Agua Potable y Redes de evacuación  de Aguas Servidas, suficientes para atender las necesidades y en el  nivel de calidad requerido por nuestros clientes.</span></p>
<p><span>En 1995 como nueva empresa concesionaria de los servicios  sanitarios de Valdivia, Aguas Décima acordó con la Superintendencia de  Servicios Sanitarios la ejecución de un Programa de Desarrollo, en el  cual se definieron una serie de obras de infraestructura tendientes a  asegurar la continuidad y la calidad de servicios para las concesiones  de producción y distribución de agua potable y recolección y disposición  de aguas servidas de esta ciudad.</span></p>

</div>

    @endsection