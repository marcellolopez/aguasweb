@extends('layouts.m2')

@section('title', 'Plantas dep. aguas servidas')
@section('empresa', 'active')
@section('planta-dep-aguas-servidas', 'active')

@section('sidebar')
@endsection


@section('content')



<div class="content">
<h2><span>Planta Depuradora de Aguas Servidas</span></h2>
<p><strong>Generalidades</strong></span><img src="https://www.aguasdecima.cl/wp-content/uploads/2010/10/edas.jpg" alt="" width="294" height="213" align="right" /></p>
<p>La Estación Depuradora de Aguas Servidas (Edas) ha sido concebida  como una instancia capaz de crecer con la ciudad, acompañando  armónicamente en su desarrollo. Las instalaciones construidas son la  primera fase de este proyecto, que contempla un tratamiento primario con  desinfección para las aguas servidas de la ciudad. En la medida que sea  necesario la intensidad del tratamiento podrá aumentar, incorporando  procesos adicionales (tratamientos Secundarios y hasta Terciarios), sin  afectar con ello el funcionamiento de esta primera etapa.</span></p>
<p><strong>Descripción  del Tratamiento<br />
</strong></span><br />
<span>El tratamiento primario con desinfección consiste básicamente en la  eliminación de los sólidos suspendidos en las aguas servidas mediante  procesos físicos de decantación. En tanto, para disminuir la  concentración de los organismos coliformes fecales, se considera una  desinfección con cloro en el efluente. Los sólidos procedentes de la  decantación primaria son estabilizados con cal, espesados y  deshidratados mecánicamente para su reutilización agrícola o disposición  final en vertedero autorizado. Para la elección del tipo y tamaño de la  instalación se tomaron en consideración tanto los aspectos técnicos y  ambientales involucrados, como los aspectos socio- económicos,  minimizando el impacto tarifario sobre los usuarios. En definitiva, se  buscó alcanzar la mejor solución desde el punto de vista técnico,  ambiental y socioeconómico para la ciudad.</span></p>
<div style="text-align: justify;"><img src="https://www.aguasdecima.cl/wp-content/uploads/2010/10/diag_edas.jpg" alt="" width="264" height="198" /></div>
<p><strong>Impactos</strong></span></p>
<p>Dentro de los indiscutibles impactos positivos que la Planta acarrea,  destaca la mejora sustancial en las aguas de los ríos Calle Calle,  Valdivia, Cau Cau y Cruces, permitiendo con ello que la ciudad tenga una  red fluvial libre de contaminación, tanto para el uso recreacional de  ésta, como para la conservación de los cientos de especies que componen  la fauna acuática. Lo anterior se traduce en un importante aporte al  desarrollo de la ciudad.</span></p>
<p><strong> Efectos ambientales de la Estación Depuradora</strong></span></p>
<p>La estación depuradora de aguas servidas Alto las  Mulatas ubicada en la ciudad de Valdivia, se basa en un tratamiento  primario con desinfección, el cual garantiza que su efluente sea  compatible con los componentes bióticos y abióticos del medio receptor,  mejorando notoriamente la cantidad microbiológica del cuerpo receptor,  esperándose encontrar en un futuro cercano, niveles de coliformes  fecales inferiores al límite exigido por la normativa, lo que permitirá  el uso del recurso hídrico sin restricción de ningún tipo.</span></p>
<p>Aguas Décima S.A. monitorea periódicamente los parámetros de operación y  funcionamiento de la EDAS., y ha contratado los estudios necesarios  tendientes a caracterizar la circulación del agua, determinar sus  características fisicoquímicas y microbiológicas, los recursos  hidrobiológicos, así como también analizar el efluente y su impacto en  el estuario.</span></p>
                    </div>
</div>

    @endsection