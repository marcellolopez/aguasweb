@extends('layouts.m2')

@section('title', 'Reseña Historica')
@section('empresa', 'active')
@section('reseña-historica', 'active')

@section('sidebar')
@endsection


@section('content')



<div class="content">
  <h1><span>Reseña Historica</span></h1>

<p><img class="" title="rhistorica-150x150" src="https://www.aguasdecima.cl/wp-content/uploads/2010/10/rhistorica-150x1501.jpg" alt="" width="148" height="148" />Después de participar en la licitación y salir adjudicada, se compro a Essal S.A., las concesiones sanitarias y los bienes muebles e inmuebles adscritos a ellas, correspondientes a la ciudad de Valdivia. Con fecha 12 de Enero de 1995 inicio las operaciones definidas en su objeto social.</p>
<p>Aguas Décima S.A. como sociedad anónima se construyó el 01 de Julio de 1994, entre Chilquinta S.A. e inmovilaria El Almendral S.A. con el objeto de crear, formar y participar en sociedades que se dedicaran a la construcción, explotación de concesiones de servicios públicos de producción y distribución de agua potable, recolección, tratamiento y disposición de aguas servidas.</p>
<p>Desde Marzo de 1995 hasta Octubre de 2006, tuvieron participación como accionistas principales Agbar Chile S.A. filial de Aguas de Barcelona España y Compañía Hispano Americana de Servicios S.A.</p>
<p>El 12 de Octubre de 2006 se celebra contrato de Compraventa de acciones, en el cual todos los accionistas de Aguas Décima S.A. es decir, La Compañia Hispanoamericana de Servicios S.A., Almendral S.A. y Agbar Chile S.A. vendieron a la Empresa Japonesa Marubeni Corporation y Marubeni Chile Limitada, el 100 % de las acciones de Aguas Décima S.A. convirtiendose en la sociedad controladora hasta el dia de hoy.</p>
<p>En sus 15 años de historia, Aguas Décima S.A. se convierte en una empresa puntera en su sector, con políticas de inversiones que hacen posible gestionar el ciclo integral del agua, además de gozar de un amplio respaldo ciudadano.</p>
<p><strong>INVERSIONES HISTÓRICAS</strong></p>

<p>Desde que Aguas Décima S.A. inició sus operaciones ha realizado importantes inversiones orientadas a mejorar los niveles de cobertura, continuidad y calidad de los productos y servicios entregados por nuestra empresa. En este contexto, a contar del año 1995, fecha en que la empresa se hizo cargo del servicio sanitario de la ciudad, se han desarrollado una serie de proyectos de manera sistemática, cuya inversión a diciembre del año 2008 alcanza una suma total que supera los $ 25.341.000.000.- ( 48,55 millones de dólares US)</p>
<p>Estas inversiones se han distribuido de la siguiente manera; El Sistema de Aguas Servidas, asociado al Plan de Saneamiento concentra el 72% del monto global, esto se debe principalmente a las inversiones para construir la Estación Depuradora de Aguas Servidas (E.D.A.S.), los Colectores Interceptores, y las obras de cobertura en alcantarillado. Por otra parte, las inversiones en el Sistema de Agua Potable tienen una participación de 15% y 4 %, en obras asociadas a la Distribución y Producción de Agua Potable respectivamente. El 8% restante está conformado por otras inversiones, principalmente en activos de operación.</p>
<img src="https://www.aguasdecima.cl/wp-content/uploads/2010/10/inversiones.jpg" alt="control" width="400" height="220" />
<p>Como objetivos principales se definieron los siguientes, según las distintas etapas del servicio:</p>
<p><strong>1.-</strong> <strong>Producción de Agua Potable:</strong></p>
<ul>
<li>Instalación de un sistema de telemetría y control, que permite un monitoreo permanente y en línea de las variables más relevantes que inciden en la producción del agua potabilizada.</li>
<li>Instalación de un sistema de Macromedición para controlar la producción.</li>
<li>Instalación de un sistema para la fluoración del Agua.</li>
<li>Optimización y renovación tecnológica de equipos.</li>
<li>Aumentar la seguridad del sistema de producción, cuya finalidad es mitigar la vulnerabilidad del sistema ante periodos de sequía prolongados y/o cortes del suministro eléctrico</li>
</ul>
<p><strong>2.-</strong> <strong>Distribución de Agua Potable:</strong></p>
<ul>
<li>Alcanzar el 100% de cobertura en el suministro de Agua Potable dentro de los límites del Territorio Operacional de la empresa.</li>
<li>Obras de capacidad y de refuerzo, para cumplir con los requisitos normativos vigentes, en cuanto volúmenes de regulación y reserva en estanques y presiones de servicio en la red.</li>
<li>Programa anual de reemplazo de redes antiguas, con el objetivo de reducir las pérdidas técnicas de Agua Potable y contribuir a mejorar la continuidad y calidad del suministro.</li>
</ul>
<p><strong>3.-</strong> <strong>Recolección de Aguas Servidas:</strong></p>
<ul>
<li>Mejorar los niveles de cobertura de alcantarillado, alcanzando a la fecha un 94%, considerando recolección con tratamiento de Aguas Servidas.</li>
<li>Programa anual de reemplazo de redes antiguas, para mejorar la continuidad del servicio de recolección de Aguas Servidas y reducir la intrusión de aguas que no sean residuales de tipo domésticas.</li>
<li>Sistema de Telemetría; que permite el monitoreo remoto de la operación de las Plantas Elevadoras de Aguas Servidas.</li>
</ul>
<p><strong>4.- Tratamiento Aguas Servidas:</strong></p>
<ul>
<li>Plan de saneamiento, que implicó la construcción de toda la infraestructura necesaria para el tratamiento de aguas servidas vertidas al Río Valdivia (Colectores Interceptores – Estación Depuradora de Aguas Servidas y Emisario Subfluvial). Obras que han contribuido a la descontaminación del río, favoreciendo así la calidad de vida de la ciudadanía Valdiviana</li>
</ul>
<p>Este nivel de inversiones históricas ha llevado a instalar aproximadamente 185.000 metros de redes, asociadas al Plan de Saneamiento, Coberturas, Renovaciones y Refuerzos del sistema.</p>
</div>

@endsection