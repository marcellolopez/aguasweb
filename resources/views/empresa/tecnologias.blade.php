@extends('layouts.m2')

@section('title', 'Tecnologías')
@section('empresa', 'active')
@section('tecnologias', 'active')

@section('sidebar')
@endsection


@section('content')



<div class="content">
<h2><span>Tecnologías</span></h2>

<p>Como empresa que busca mantenerse a la cabeza hemos incorporado  siempre los últimos adelantos tecnológicos, buscando así mejorar la  calidad de nuestro producto y servicios, tanto en las plantas de  tratamiento, depuradoras, laboratorios y en la atención de nuestros  clientes.</p>
<div>
<p><strong><br />
<span>Comando a distancia </strong></p>
<p>Un sistema de Telemando y Telecontrol nos  permite conocer el  estado de las instalaciones en tiempo real y poder actuar  sobre ellas,  gracias a ello podemos conocer el nivel de los estanques, las  presiones  en la red y el accionamiento de las válvulas.</p>
<div><img src="https://www.aguasdecima.cl/wp-content/uploads/2010/10/lab2.jpg" alt="control" width="260" height="118" /></div>
<p>En la actualidad contamos con 2 sistemas de  Telemando, uno de  ellos nos permite monitorear y controlar el proceso de  Captación y  Tratamiento de Agua Potable y el otro el monitoreo de las Plantas  Elevadoras  de Aguas  Servidas.</p>
<p>El Sistema de telemetría de Agua Potable une 6  estaciones remotas,  mientras que el sistema de Aguas Servidas la conforman 23 estaciones   remotas.</p>
<p>Los datos emitidos por las estaciones son  recogidos en tiempo real y  visualizados en el Centro de Control y Monitoreo por  las aplicaciones  SCADA, encargadas de procesar y almacenar los datos. Logrando  así tener  el control del estado de las Redes, la actuación sobre los elementos   que la componen y el almacenamiento de datos históricos para consultas   posteriores de los mismos.</p>
<div><img src="https://www.aguasdecima.cl/wp-content/uploads/2010/10/Pantalla.jpg" alt="" width="272" height="204" /></div>
<p><span><strong>Modelamiento de Agua Potable </strong></p>
<p>A comienzo de 1999 se  desarrollo un proyecto pionero denominado  “Modelo Matemático para la simulación  hidráulica de la red de Agua  Potable”<br />
En términos sencillos  podemos decir que un Modelo Matemático, no es  otra cosa que una simulación que  representa el comportamiento  hidráulico de nuestra red de abastecimiento, con  la finalidad de  predecir en el tiempo el comportamiento de ciertos parámetros  de la Red  de  Agua, como son por ejemplo; los caudales , las presiones,  velocidad, etc.<br />
Entre los principios  que determinaron la puesta en marcha de este proyecto podemos mencionar los  siguientes</p>
<p>•  Lograr una Optima gestión del abastecimiento.<br />
•  Necesidad de Diagnostico de una Red de Agua  Potable.<br />
•  Necesidades de Explotación de una Red de Agua  Potable (Puntos de conexión a nuevos<br />
proyectos, simular proyecciones de  crecimiento).<br />
•  Planificación de refuerzos necesarios en    la red  para el corto y mediano plazo.<br />
•  Detección de singularidades (perdidas o fugas) no registradas en la red.</p>
<div><img src="https://www.aguasdecima.cl/wp-content/uploads/2010/10/modelomatematico.jpg" alt="" width="284" height="204" /></div>
<p>
<p><span><strong>Sistema gestión de redes</strong></p>
<p>Para realizar una óptima gestión del suministro y distribución del   Agua Potable, y la recolección y disposición de las aguas servidas, es   necesario conocer de manera detallada la información asociada de la   infraestructura sanitaria.<br />
Es por ello que surge la necesidad de implementar esta  herramienta,  cuyo resultado final es fruto de un arduo trabajo que comenzó el   segundo semestre del año 1996, para finalmente iniciar su operación a  mediados  del año 1999.<br />
En virtud de lo anterior podemos definir un G.I.S. como una  potente  herramienta informática compuesta por Computadoras (Hardware) y   Programas (Software) que permiten visualizar, almacenar, capturar,  chequear y  manipular de manera inteligente la infraestructura de  nuestra empresa.<br />
Dentro de las aplicaciones que  podemos obtener esta herramienta podemos mencionar las siguientes:</p>
<p>•  Gestión patrimonial de la  red<br />
•  Mantener  actualizados los Inventarios.<br />
•  Consulta  sobre datos de cualquier elemento de la red.<br />
•  Planificación  de Obras (Renovaciones por antigüedad, estado de la red, etc).<br />
•  Análisis de conectividad de  la red.<br />
•  Simulación  de averías.<br />
•  Visualización en pantalla  de una determinada zona del territorio.<br />
•  Ubicación  de elementos determinados a partir de una dirección (nombre de calle y<br />
número)Visualización de clientes.</p>
<div><img src="https://www.aguasdecima.cl/wp-content/uploads/2010/10/gis.jpg" alt="" width="292" height="204" /></div>
</div>

    @endsection