@extends('layouts.m2')

@section('title', 'Departamentos')
@section('empresa', 'active')
@section('departamentos', 'active')

@section('sidebar')
@endsection


@section('content')



  <div class="content">
  	<h1><span>Departamentos</span></h1>
                                       
<p><strong><span>Nuestra empresa esta constituida por los siguientes departamentos.</span></strong></p>
<p><strong><span>Departamento de Administración General</span></strong></p>
<p><span>Este departamento elabora políticas financieras y se encarga de la gestión de logística, Recursos humanos y comerciales de  la organización.</span></p>
<p><strong><span>Departamento Operaciones</span></strong></p>
<p><span>Es el que gestiona el ciclo del agua desde la potabilización como agua potable hasta la disposición como aguas servidas, asegurando un producto de calidad,  en presión, continuidad y apto para el consumo en el caso del agua potable y  mantiene en correcto estado y funcionamiento.</span></p>
<p><strong><span>Asesoria de Calidad</span></strong></p>
<p><span>Coordina la implementación del Sistema de Calidad, lo que lleva a organizar auditorias internas y externas. Planifica y supervisa todas las actividades de gestión de los laboratorios de la empresa y la gestión de RILES ( residuos industriales líquidos ), y el cumplimiento de los estándares de calidad del agua potable y de las aguas servidas tratadas.</span></p>
<p><span><strong>Departamento de Planificación y Control</strong></span></p>
<p><span>Es el responsable del cumplimiento de los Programas de Desarrollo, para la Superintendencia de Servicios Sanitarios, a su vez, controla la documentación e informes dirigidos tanto a la SISS como a otros organismos estatales y privados, además coordina, supervisa y prepara los diversos Proyectos de Ingeniería, y estudios de planificación, orientados al crecimiento de la empresa, en el corto, mediano y largo plazo, lo que implica una participación activa en el control global de Inversiones de la empresa.</span></p>  
<p><span>Aguas D&eacute;cima S.A. 2013 <a href="https://www.aguasdecima.cl" target="_self">Empresa de servicios Sanitarios de Valdivia</a></span></p>
</div>

@endsection