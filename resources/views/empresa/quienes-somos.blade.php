@extends('layouts.m2')

@section('title', '¿Quienes somos?')
@section('empresa', 'active')
@section('quienes-somos', 'active')

@section('sidebar')
@endsection


@section('content')



<div class="content">
  <h1><span>¿Quienes somos?</span></h1>
  <p>Aguas Décima S.A. es la empresa de servicios sanitarios de la ciudad de Valdivia, donde opera desde el año 1995.</p>
  <p>Aguas Décima S.A. gestiona el ciclo integral del agua, es decir, la captación, producción y distribución de agua potable, como también, la recolección, tratamiento y disposición de las aguas servidas, en cumplimiento al Programa de Monitoreo establecido por la Superintendencia de Servicios Sanitarios y obedeciendo a la norma de emisión, Decreto Supremo SEGPRES Nº 90/2000.</p>
  <p>Aguas Décima S.A. mantiene como filosofía empresarial una decidida apuesta por la mejora continua en sus procesos y el respeto por el medio ambiente.</p>
  <p>Paralelamente Aguas Décima S.A. mantiene una voluntad de diálogo y transparencia con sus partes interesadas como son los clientes, accionistas, empleados y la opinión pública en general.</p>
  <p>En agosto de 2014 Aguas Décima S.A. proveía agua potable a 42984 clientes, en tanto que los de alcantarillado eran 40689.</p>
  <div><strong>Visión<br />
  </strong>&#8220;Ser la mejor empresa de servicios de la región de los rios&#8221;<br />
  <strong><br />
    Misión<br />
  </strong></div>
  <p>&#8220;Prestar servicios sanitarios en la provincia de Valdivia, logrando la satisfacción de nuestros clientes, maximizando los beneficios económicos a largo plazo y contribuyendo, de igual modo, a la mejora del medio ambiente en la provincia y al desarrollo personal de todos los trabajadores de la empresa.”</p>
</div>

@endsection