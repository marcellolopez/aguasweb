@extends('layouts.m2')

@section('title', 'Politicas de Calidad')
@section('empresa', 'active')
@section('politicas-de-calidad', 'active')

@section('sidebar')
@endsection


@section('content')



<div class="content">
<h2><span>Políticas Calidad</span></h2>
<div class="col-md-8" style="text-align: center;"><a href="https://www.aguasdecima.cl/wp-content/uploads/2010/10/Politica_Calidad.jpg"><img class="aligncenter size-large wp-image-463" title="Politica_Calidad" src="https://www.aguasdecima.cl/wp-content/uploads/2010/10/Politica_Calidad-773x1024.jpg" alt="" width="477" height="811" /></a></div>

<p>La empresa AGUAS DECIMA S.A. ejecuta sus operaciones bajo un modelo de calidad cuyo núcleo esencial y estructura está descrita en un Manual de Calidad y en un conjunto de documentos, datos y registros, que cada uno de los Departamentos y Unidades controla y gestiona.
<br />
Este modelo representa una forma de normalizar las mejores prácticas dentro de nuestra Organización, asegurando una adecuada estandarización en nuestros métodos de trabajo, así como una herramienta de mejora continua, intercambio de experiencias y colaboraciones entre todo el personal, lo que ha demostrado su validez en el tiempo.</p>                   
</div>

    @endsection