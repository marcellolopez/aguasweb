@extends('layouts.m2')

@section('title', 'Laboratorios')
@section('empresa', 'active')
@section('laboratorios', 'active')

@section('sidebar')
@endsection


@section('content')



<div class="content">
<h2><span>Laboratorios</span></h2>
<p><img src="https://www.aguasdecima.cl/wp-content/uploads/2010/10/lab.jpg" alt="laboratorios" width="164" height="251" align="left" /><span>El Laboratorio de Control de Calidad se ocupa de controlar periódicamente la calidad del producto entregado por Aguas Décima S.A. a sus clientes para garantizar y asegurar la máxima calidad del mismo y su adecuación a los estándares y normativas exigibles, tanto en aguas de consumo público como en aguas depuradas.</span><br />
<span>Sus actividades se desarrollan en el control fisicoquímico y bacteriológico de aguas, control de procesos de potabilización, control de procesos de depuración, seguimiento y control de vertidos industriales al alcantarillado, como el seguimiento, control de subproductos y fangos de depuración de aguas.<br />
Se dispone de dos Laboratorios, uno instalado en la  Planta Edas y dedicado mayoritariamente al control de proceso en ésta, dada su envergadura y complejidad, y el segundo ubicado en la Etap de Cuesta de Soto que se dedica al resto de actividades llevadas a cabo, y donde se encuentran el grueso del soporte analítico disponible en los Laboratorios de Aguas Décima S.A.</span></p>
<p><span>El Laboratorio de Ensayo cuenta con la acreditación vigente, dentro del Sistema Nacional de Acreditación del INN, como Laboratorio de Ensayo bajo la norma NCh-ISO 17.025 Of 2005, para las áreas de Microbiología de aguas, (Certificado LE-114) y Físico-química para aguas, (Certificados LE-115), para el análisis de muestra de agua potable, de las fuentes de captación, aguas crudas y aguas residuales.<br />
Los parámetros acreditados en el área de microbiología, corresponden a Coliformes Totales, Coliformes Fecales y <em>Escherichia coli</em>.<br />
Para el área físico química los parámetros son: Turbiedad, color, olor, sabor, pH, Temperatura, Conductividad y Fluoruro.</span></p>
<p><span>El laboratorio presta servicios de análisis de agua a terceros según el alcance de la acreditación bajo el INN.</span></p>
<p><span>Finalmente, la plantilla técnica de los Laboratorios está integrada por técnicos con alta calificación que le permiten acometer todas las actividades y estudios mencionados con anterioridad.</span></p>
<p><img class="aligncenter" src="https://www.aguasdecima.cl/wp-content/uploads/2010/10/lab_2.jpg" alt="" width="235" height="176" /><br />
﻿</p>
</div>

    @endsection