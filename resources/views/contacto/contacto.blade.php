

@extends('layouts.m1')

@section('title', 'Contacto')
<meta name="csrf-token" content="{{ csrf_token() }}" />
@section('contacto', 'active')

@section('sidebar')




@section('content')
<section class="">
  <div class="container">
 <?php 
  if (isset($mensaje))
  {
    echo $mensaje;
  } 
 ?>

  <h1><span> Contáctenos:</span></h1>
  <br>
  <form method="post" id="frmIngresarReclamo" name="frmIngresarReclamo">
    <!-- Select Basic -->
    <div class="control-group">
      <label class="control-label" for="selectbasic">Motivo:</label>
      <div class="controls ">
        <select id="ddMotivos" name="ddMotivos"  class="input-xlarge form-control">
          @foreach($motivos as $motivos)
          @if ($motivos->estado=='1')
          <option id="" name="" value="{{$motivos->id}}">{{ucfirst($motivos->nombre)}}</option>
          @endif
          @endforeach
        </select>
        <br>

    <div class="control-group">
      <label class="control-label" for="mensaje">Nombre:</label>
      <div class="controls">                     
        <input type="text"  id="txtNombre" name="txtNombre"  class="input-xlarge form-control" onkeypress="return soloLetras(event)">
        <label id="lblNombre" class="control-label-error text-warning" for="inputError"></label>    
      </div>
    </div>
    <br>

    <div class="control-group">
      <label class="control-label" for="mensaje">Apellido Paterno:</label>
      <div class="controls">                     
        <input type="text"  id="txtApellidoPaterno" name="txtApellidoPaterno"  class="input-xlarge form-control" onkeypress="return soloLetras(event)">
        <label id="lblApellido_Paterno" class="control-label-error text-warning" for="inputError"></label>
      </div>
    </div>
    <br>
    
    <div class="control-group">
      <label class="control-label" for="mensaje">Apellido Materno:</label>
      <div class="controls">                     
        <input type="text"  id="txtApellidoMaterno" name="txtApellidoMaterno"  class="input-xlarge form-control" onkeypress="return soloLetras(event)">
        <label id="lblApellido_Materno" class="control-label-error text-warning" for="inputError"></label>
      </div>
    </div>
    <br>

    <div class="control-group">
      <label class="control-label" for="mensaje">Rut:</label>
      <div class="controls">                     
        <input type="text"  id="txtRut" name="txtRut"  class="input-xlarge form-control" placeholder="Ej:1222333-k" onblur="onRutBlur(value)" onkeypress="return soloNumyk(event)">
        <label id="lblRut" class="control-label-error text-warning" for="inputError"></label>
      </div>
    </div>
    <br>

    <div class="control-group">
      <label class="control-label" for="mensaje">Número de servicio:</label>
      <div class="controls">                     
        <input type="text"  id="txtNumero_servicio" name="txtNumero_servicio"  class="input-xlarge form-control">
        <label id="lblNumero_Servicio" class="control-label-error text-warning"  for="inputError"></label>
      </div>
    </div>
    <br>

    <div class="control-group">
      <label class="control-label" for="mensaje">Dirección:</label>
      <div class="controls">                     
        <input type="text"  id="txtDireccion" name="txtDireccion"  class="input-xlarge form-control">
        <label id="lblDireccion" class="control-label-error text-warning" for="inputError"></label>
      </div>
    </div>
    <br>

    <div class="control-group">
      <label class="control-label" for="mensaje">E-mail:</label>
      <div class="controls">                     
        <input type="text"  id="txtEmail" name="txtEmail"  class="input-xlarge form-control">
        <label id="lblEmail" class="control-label-error text-warning" for="inputError"></label>
      </div>
    </div>
    <br>

            <div class="control-group">
              <div class="controls">
                <label class="control-label" for="txtTelefono">Teléfono de contacto:</label> <input type="radio" id="fono1" name="fono" value="fijo" onclick="fijo()">  Fijo   <input type="radio" id="fono2" name="fono" value="movil" onclick="movil()">  Movil
                <div class="input-group">
                  <input type="hidden" id="codigo" name="codigo" value="" >
                <span class="input-group-addon" style="border-radius: 10px 0 0 10px;
  background-color: white;
  color: black;
  border-color: rgb(69, 104, 121);
  border-right-style: none;" name="basic-addon1" id="basic-addon1"></span>
                <input id="txtTelefono" maxlength="10" name="txtTelefono" type="text" placeholder="" class="form-control"  onkeypress="return soloNum(event)" disabled="disabled" aria-describedby="basic-addon1">  
                </div>
                <label id="lblTelefono"  class="control-label-error text-warning"  for="inputError"></label>
              </div>
            </div>
    <br>

    <!-- Textarea -->
    <div class="control-group">
      <label class="control-label" for="mensaje">Mensaje:</label>
      <div class="controls">                     
        <textarea id="cmbMensaje" name="cmbMensaje" maxlength="500" class="form-control taReclamo"></textarea>
      </div>
    </div>
    <br>
    <!-- Button -->
    
    <input type="hidden" name="id_motivo" id="id_motivo">
    <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
  </form>
  <div class="control-group">
    <div class="controls">
      <button id="ingresar" onclick="valReclamo()" name="ingresar" value="" class="btn btn-primary form-control">Ingresar reclamo</button>
      <label id="lblingresar" class="control-label-error text-warning" for="inputError"></label>                
    </div>
  </div>
  </div>
</section>  
         
@endsection

