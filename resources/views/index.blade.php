

@extends('layouts.m1')

@section('title', 'Inicio')

@section('inicio', 'active')

@section('sidebar')


@section('content')
 <!-- Component: index/info-thumbnails-alt2.html - START -->
    <section class="">
      <div class="container">
        <h1><span>Noticias</span></h1>
        <div class="row">
          <div class="col-sm-4 col-md-4">
            <div class="info-thumbnail-link">
            <div class="thumbnail info-thumbnail background-clouds color-black">
              <img src="img/img1.jpg" alt="...">
              <div class="caption">
                <h3>Sobre la calidad del agua...</h3>
                <p class="description">Para la tranquilidad de nuestros clientes y usuarios en general, se informa que Aguasdecima realiza controles diarios y permanentes de la calidad del Agua Potable en la red de Distribución, de acuerdo a lo establecido en la Normativa vigente aplicable al agua potable.</p>
                <p class="buttons"><a href="#" class="btn btn-lead">Read more</a></p>
              </div>
            </div>
            </div>
          </div>
          <div class="col-sm-4 col-md-4">
            <div class="info-thumbnail-link">
            <div class="thumbnail info-thumbnail background-clouds color-black">
              <img src="img/img1.jpg" alt="...">
              <div class="caption">
                <h3>Font awesome</h3>
                <p class="description">NEAT template supports as much modern technologies and trends as possible. One of them is font awesome icons. This great set of fonts is avaliable anywere you want on your page. Some of NEAT ui elements are also using font awesome like this info thumbnails.</p>
                <p class="buttons"><a href="#" class="btn btn-lead">Read more</a></p>
              </div>
            </div>
            </div>
          </div>
          <div class="col-sm-4 col-md-4">
            <div class="info-thumbnail-link">
            <div class="thumbnail info-thumbnail background-clouds color-black">
              <img src="img/img1.jpg" alt="...">
              <div class="caption">
                <h3>Font awesome</h3>
                <p class="description">NEAT template supports as much modern technologies and trends as possible. One of them is font awesome icons. This great set of fonts is avaliable anywere you want on your page. Some of NEAT ui elements are also using font awesome like this info thumbnails.</p>
                <p class="buttons"><a href="#" class="btn btn-lead">Read more</a></p>
              </div>
            </div>
            </div>
          </div>
        </div>
    </div>      
    </section>
    <!-- Component: index/info-thumbnails-alt2.html - END -->    
@endsection

