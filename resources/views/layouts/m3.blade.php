<!DOCTYPE html>
<html>
  <head>
    <title>Aguasdecima -  </title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">    
    <link rel="stylesheet" href="css/neat-blue.css" media="screen" id="neat-stylesheet">

    <link rel="stylesheet" href="css/blueimp-gallery.min.css">
    <link rel="stylesheet" href="libs/Bootstrap-Image-Gallery-3.1.1/css/bootstrap-image-gallery.min.css">

    <!-- Use google font -->
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700,300italic,400italic,700italic|Lustria" rel="stylesheet" type="text/css" />

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="background-clouds">
    <div id="nav-wrapper" class="background-white color-black">
      <nav id="mainMenu" class="navbar navbar-fixed-top" role="navigation">
        <div class="container">
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar background-lead"></span>
              <span class="icon-bar background-lead"></span>
              <span class="icon-bar background-lead"></span>
            </button>
            <a class="navbar-brand" href="index.html"><img src="img/logo.png" alt="logo"></a>
          </div>

          <!-- Collect the nav links, forms, and other content for toggling -->
          <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav navbar-right">   

              <li><a href="inicio">Inicio</a></li> 

              <li class="dropdown @yield('empresa')" >
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Empresa <b class="caret"></b></a>
                <ul class="dropdown-menu">
                  <li ><a href="quienes-somos">¿Quienes somos?</a></li>
                  <li ><a href="resena-historica">Reseña historica</a></li>
                  <li ><a href="datos-de-empresa">Datos de empresa</a></li>
                  <li ><a href="departamentos">Departamentos</a></li>
                  <li ><a href="plantas-de-agua-potable">Plantas de agua potable</a></li>
                  <li ><a href="planta-dep-aguas-servidas">Planta dep. aguas servidas</a></li>
                  <li ><a href="distribucion">Distribución</a></li>
                  <li ><a href="laboratorios">Laboratorios</a></li>
                  <li ><a href="politicas-de-calidad">Politicas de calidad</a></li>
                  <li ><a href="tecnologias">Tecnologias</a></li> 
                  <li ><a href="inversiones">Inversiones</a></li>                                                                        
                  <li ><a href="memorias-y-eeff">Memorias y EEFF</a></li>                                                                                                                                              
                </ul>
              </li>

              <li class="dropdown  @yield('servicio-cliente')">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Servicio al cliente <b class="caret"></b></a>
                <ul class="dropdown-menu">
                  <li><a href="tarifas">Tarifas</a></li>
                  <li><a href="verificar-documento-electronico">Verificar documento electronico</a></li>
                  <li><a href="reglamentacion-y-compromiso">Reglamentación y compromiso</a></li>
                  <li><a href="centro-de-atencion-al-cliente">Centro de atención al cliente</a></li>
                  <li><a href="centro-de-pagos">Centro de pagos</a></li>
                  <li><a href="subsidio">Subsidio</a></li>
                  <li><a href="contratistas-autorizados">Contratistas autorizados</a></li>
                  <li><a href="preguntas-frecuentes">Preguntas frecuentes</a></li>
                  <li><a href="conozca-su-boleta">Conozca su boleta</a></li>
                  <li><a href="proveedores">Proveedores</a></li> 
                  <li><a href="solicitudes-de-factibilidad">Solicitudes de factibilidad</a></li>                                                                        
                </ul>
              </li>

              <li class="dropdown @yield('articulos')">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Articulos <b class="caret"></b></a>
                <ul class="dropdown-menu">
                  <li><a href="cortes-programados">Cortes programados</a></li>
                  <li><a href="cortes-de-emergencia">Cortes de emergencia</a></li>
                  <li><a href="noticias">Noticias</a></li>
                  <li><a href="tareas-escolares">Tareas escolares</a></li>
                  <li><a href="consejos-para-ahorrar-agua">Consejos para el ahorro de agua</a></li>
                </ul>
              </li>
              <li class="dropdown @yield('contacto')"><a href="contacto">Contacto</a></li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="fa fa-search"></span></a>
                <ul class="dropdown-menu">
                  <li>
                    <form class="form-inline" role="form">
                      <div class="input-group">
                        <input type="text" class="form-control">
                        <span class="input-group-btn">
                          <button class="btn btn-default" type="submit">Search</button>
                        </span>
                      </div>
                    </form>
                  </li>
                </ul>
              </li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="fa fa-lock"></span></a>
                <ul class="dropdown-menu">
                  <li>
                    <form role="form">
                      <div class="form-group">
                        <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
                      </div>
                      <div class="form-group">
                        <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                      </div>
                      <div class="checkbox">
                        <label>
                          <input type="checkbox"> Remember me
                        </label>
                      </div>
                      <button type="submit" class="btn btn-default">Login</button>
                      <span class="color-white"><button type="button" class="btn btn-link pull-right">Register</button></span>
                    </form>
                  </li>
                </ul>
              </li>
            </ul>
          </div><!-- /.navbar-collapse -->
        </div>
      </nav>


    </div>
    <!-- Breadcrumbs - START -->
    <div class="breadcrumbs-container">
      <div class="container">
      </div>
    </div>
    <!-- Breadcrumbs - END -->

    <!-- Component: aboutus/aboutus.html - START -->
    <section class="">
      <div class="container">
        <div class="row">
          <div class="col-md-3">
            <div class="list-group bright background-clouds color-text">
              <div class="list-group-item"><h3>Servicio al cliente</h3></div>
                  <a href="tarifa"  class="list-group-item @yield('tarifa')">Tarifa</a>
                  <a href="verificar-documento-electronico"  class="list-group-item @yield('verificar-documento-electronico')">Verificar Documento Electronico</a>
                  <a href="reglamentacion-y-compromiso"  class="list-group-item @yield('reglamentacion-y-compromiso')">Reglamentación y compromiso</a>
                  <a href="centro-de-atencion-al-cliente"  class="list-group-item @yield('centro-de-atencion-al-cliente')">Centro de atención al cliente</a>
                  <a href="centro-de-pagos"  class="list-group-item @yield('centro-de-pagos')">Centro de pagos</a>
                  <a href="subsidio"  class="list-group-item @yield('subsidio')">Subsidio</a>
                  <a href="contratistas-autorizados"  class="list-group-item @yield('contratistas-autorizados')">Contratistas autorizados</a>
                  <a href="preguntas-frecuentes"  class="list-group-item @yield('preguntas-frecuentes')">Preguntas frecuentes</a>
                  <a href="conozca-su-boleta"  class="list-group-item @yield('conozca-su-boleta')">Conozca su boleta</a>
                  <a href="proveedores"  class="list-group-item @yield('poveedores')">Proveedores</a>                                        
                  <a href="solicitudes-de-factibilidad"  class="list-group-item @yield('solicitudes-de-factibilidad')">Solicitudes de factibilidad</a>   
          </div>
        </div>
          <div class="col-md-9">
            @yield('content')
          </div>
        </div>      </div>
    </section>
   


    <footer class="background-midnight-blue color-white">
      <div class="container">
        <div class="row">
          <div class="col-md-3">
            <h3>Menu</h3>
            <ul class="nav-footer">
              <li class="active"><a href="#">Home</a></li>
              <li><a href="#">About us</a></li>
              <li><a href="#">Pages</a></li>
              <li><a href="#">Blog</a></li>
              <li><a href="#">Portfolio</a></li>
              <li><a href="#">Contact</a></li>
            </ul>
          </div>
          <div class="col-md-4">
            <h3>AguasDecima S.A.</h3>
            <p class="testimonial">Empresa de servicios Sanitarios de Valdivia</p>
            <p class="testimonial">Casa comercial - Arauco #434 Valdivia</p>
            <p class="testimonial">(63)2211109 - (63)2213322</p>
            <p class="testimonial">8:30 - 13:00 / 14:30 - 18:00</p>
          </div>
          <div class="col-md-5">
            <h3>Contactenos</h3>
            <form class="form-horizontal" role="form">
              <div class="form-group">
                <label for="inputName1" class="col-lg-3 control-label">Nombre</label>
                <div class="col-lg-9">
                  <input type="text" class="form-control input-lg" id="inputName1" placeholder="Name">
                </div>
              </div>
              <div class="form-group">
                <label for="inputEmail1" class="col-lg-3 control-label">Email</label>
                <div class="col-lg-9">
                  <input type="email" class="form-control input-lg" id="inputEmail1" placeholder="Email">
                </div>
              </div>
              <div class="form-group">
                <label for="inputContent1" class="col-lg-3 control-label">Mensaje</label>
                <div class="col-lg-9">
                  <textarea id="inputContent1" class="form-control" rows="3"></textarea>
                </div>
              </div>
              <div class="form-group">
                <div class="col-lg-offset-3 col-lg-9">
                  <button type="submit" class="btn btn-lead btn-lg">Enviar</button>
                </div>
              </div>
            </form>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <hr>
            <div class="row">
              <div class="col-md-6">
                Aguas Décima S.A. &copy; 2015 
              </div>
              <div class="col-md-6">
                <p class="social">
                  <a href="/"><span class="fa fa-facebook"></span></a>
                  <a href="/"><span class="fa fa-twitter"></span></a>
                  <a href="/"><span class="fa fa-youtube"></span></a>
                  <a href="/"><span class="fa fa-linkedin"></span></a>
                  <a href="/"><span class="fa fa-pinterest"></span></a>
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </footer>

    <!-- The Bootstrap Image Gallery lightbox, should be a child element of the document body -->
    <div id="blueimp-gallery" class="blueimp-gallery">
      <!-- The container for the modal slides -->
      <div class="slides"></div>
      <!-- Controls for the borderless lightbox -->
      <h3 class="title">title</h3>
      <a class="prev">‹</a>
      <a class="next">›</a>
      <a class="close">×</a>
      <a class="play-pause"></a>
      <ol class="indicator"></ol>
      <!-- The modal dialog, which will be used to wrap the lightbox content -->
      <div class="modal fade">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" aria-hidden="true">&times;</button>
              <h4 class="modal-title">title</h4>
            </div>
            <div class="modal-body next"></div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default pull-left prev">
                <i class="glyphicon glyphicon-chevron-left"></i>
                Previous
              </button>
              <button type="button" class="btn btn-primary next">
                Next
                <i class="glyphicon glyphicon-chevron-right"></i>
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>

    <!-- Include slider -->
    <script src="js/jquery.event.move.js"></script>
    <script src="js/jquery.mixitup.min.js"></script>
    <script src="js/responsive-slider.js"></script>
    <script src="js/responsive-calendar.js"></script>
    <script src="js/jquery.blueimp-gallery.min.js"></script>
    <script src="libs/Bootstrap-Image-Gallery-3.1.1/js/bootstrap-image-gallery.min.js"></script>
    <script src="js/reduce-menu.js"></script>
    <script src="js/match-height.js"></script>

    <script type="text/javascript">
    $(document).ready(function() {
      $('.responsive-calendar').responsiveCalendar({
        time: '2013-05',
        events: {
          "2013-05-30": {"number": 5, "badgeClass": "background-turquoise", "url": "http://w3widgets.com/responsive-slider"},
          "2013-05-26": {"number": 1, "badgeClass": "background-turquoise", "url": "http://w3widgets.com"}, 
          "2013-05-03": {"number": 1, "badgeClass": "background-pomegranate"}, 
          "2013-05-12": {}}
      });

      $('#mixit').mixitup();
    });

    $(window).load(function(){
      if ($(window).width() > 767) {
        matchHeight($('.info-thumbnail .caption .description'), 3);
      }

      $(window).on('resize', function(){
        if ($(window).width() > 767) {
          $('.info-thumbnail .caption .description').height('auto');
          matchHeight($('.info-thumbnail .caption .description'), 3);
        }
      });
    });
    </script>
  </body>
</html>