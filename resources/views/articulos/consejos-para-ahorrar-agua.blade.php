@extends('layouts.m4')

@section('title', 'Consejos para ahorrar agua')
@section('articulos', 'active')
@section('consejos-para-ahorrar-agua', 'active')

@section('sidebar')
@endsection


@section('content')



<div class="content">
 <h2><span>Consejos para ahorrar agua</span></h2>
<p>Tome parte activa en el ahorro de agua. Tenga en cuenta  que SÓLO EL 1% DEL AGUA DE LA   TIERRA ES APTA PARA BEBER.</p>
<ol>
<li>Cierre la llave mientras se esté  cepillando los dientes</li>
<li>Con una ducha rápida gastará menos agua que si llena la  bañera.</li>
<li>Antes de poner en marcha la lavadora espere a que esté  completamente llena de ropa.</li>
<li>No descongele los alimentos poniéndolos debajo del chorro  de agua. Sáquelos del congelador la noche anterior.</li>
<li>Cierre bien todas las llaves antes de acostarse para que  no goteen.</li>
<li>Riegue el jardín menos frecuentemente pero más  cuidadosamente.</li>
<li>Si necesita verter agua ¡no la desperdicie virtiéndola  sobre una superficie seca!. Aprovéchela para regar las plantas.</li>
<li>Arregle los inodoros, llaves y tuberías que presenten  fugas.</li>
<li>Descargue el agua de los inodoros solamente cuando sea  necesario.</li>
</ol></div>

@endsection