@extends('layouts.m4')

@section('title', 'Tareas Escolares')
@section('articulos', 'active')
@section('tareas-escolares', 'active')

@section('sidebar')
@endsection


@section('content')



<div class="content">
<h2><span>Tareas Escolares</span></h2>

<p><span><strong>El Ciclo del Agua</strong></span></p>
<div>
<p><span><strong>¿Cuánta agua hay en la tierra?</strong></span></p>
<p>La mayoría de la tierra se compone de agua, hay mucha más agua que tierra.<br />
Cerca del 70% de la superficie de la tierra está cubierta por agua.  Pero el agua también existe en el aire como vapor y en acuíferos en el  suelo, como agua subterránea. El total de agua en el mundo es  1.400.000.000 km3. (Un km3 agua es igual a un trillón de litros.) Cerca  de 3.100 Km3 de agua se puede encontrar en la atmósfera como vapor de  agua. Cada día, 280 km3 de agua se evaporan en la atmósfera.<br />
Del agua dulce que hay en la tierra, más de 100.000 km3 se almacenan  en el suelo, sobre todo dentro de la mitad de la milla de la superficie.  También se sabe que 10.500.000 km3 de agua están almacenados como agua  dulce en los lagos, los humedales y las aguas corrientes. La mayoría del  de agua dulce se almacena en glaciares y capas de hielo, principalmente  en las regiones polares y en Groenlandia. Esto son otros 24.500.000 km3  de agua.</p>
<p><span><strong>¿Cuánta agua hay en los océanos?</strong></span></p>
<p>Los océanos almacenan la mayor cantidad de agua de la tierra,  aproximadamente el 97% de la cantidad total de agua que hay sobre la  tierra y el 2% de la cual está congelada.</p>
<p><span><strong>¿Qué cantidad de agua dulce hay disponible?</strong></span></p>
<p>De toda el agua que hay en la tierra, el 97.14% de la cantidad total  del agua superficial, sólo el 2.59% es agua dulce. De este 2.59% otro  porcentaje está atrapado en forma de casquetes polares, que es 2%. El  resto de este agua dulce es agua subterránea (el 0.592%), o es agua  fácilmente accesible en lagos, aguas corrientes, ríos, etc. (el 0.014%).</p>
<p><span><strong>¿Cuánta cantidad de agua potable hay disponible?</strong></span></p>
<p>Para las cantidades que se han mencionado arriba, se puede concluir  que menos del 1% del agua existente sobre la tierra puede ser usada como  agua potable.</p>
<p><span><strong>¿Qué cantidad del cuerpo humano está formada por agua?</strong></span></p>
<p>Los seres humanos consisten principalmente en agua; el agua está en  todos nuestros órganos y es transportada a todas partes de nuestro  cuerpo para ayudar en funciones físicas. Cuando un ser humano no absorbe  suficiente agua, el resultado es la deshidratación. Esto no es muy  sorprendente, ya que el 66% del cuerpo humano consiste en agua.</p>
<p><span><strong>¿Qué causa escasez de agua dulce?</strong></span></p>
<p>Hay cuatro causas deferentes para la escasez de agua: un clima seco,  sequía (periodo en el cual las precipitaciones son muy bajas y la  evaporación es más alta de lo normal), secar el suelo debido a  actividades como la deforestación y el sobre pastoreo de la ganadería y  el estrés del agua debido al incremento del número de gente que depende  de un nivel limitado de agua corriente.</p>
<p><span><strong>¿Cómo se pueden aumentar los abastecimientos de agua?</strong></span></p>
<p>Hay cinco maneras de aumentar abastecimientos de agua en un área  particular: presas y depósitos para almacenar el agua de salida, traer  el agua superficial de otra área, sacar agua subterránea, convertir el  agua salada en agua dulce (desalinización) y mejorar la eficacia del uso  del agua. Todos Estos métodos tienen sus pros y contras.</p>
<img class="img-responsive" src="http://www.aguasdecima.cl/wp-content/themes/aguasdecima/images/ciclo_agua.jpg">
<p><span>1) Precipitación</span><br />
Transporte de agua hacia el interior de las nubes con movimientos  circulares y que como resultado de la gravedad cae en la tierra  condensada como agua. Este fenómeno se llama lluvia o precipitación. <a href="http://www.ina.gov.ar/cartillas_edu/cartilla_3.htm#b#b"><br />
</a><br />
<span>2) Infiltración</span><br />
El agua de lluvia se infiltra en la tierra y se hunde en la zona  saturada, donde se convierte en agua subterránea. El agua subte-rránea  se mueve lentamente desde lugares con alta presión y elevación hacia los  lugares con una baja presión y elevación. Se mueve desde el área de  infiltración a través de un acuífero y hacia un área de descarga, que  puede ser un mar o un océano.</p>
<p><span>3) Transpiración</span><br />
Las plantas y otras formas de vegetación toman el agua del suelo y  la excretan otra vez como vapor de agua. Cerca del 10% de la  precipitación que cae en la tierra se vaporiza otra vez a través de la  transpiración de las plantas, el resto se evapora de los mares y de los  océanos.</p>
<p><span>4) Salida superficial</span><br />
El agua de lluvia que no se infiltra en el suelo alcanzará  directamente el agua superficial, como salida a los ríos y a los lagos.  Después será transportada de nuevo a los mares y a los océanos. Esta  agua es llamada agua de salida superficial.</p>
<p><span>5 ) Evaporación</span><br />
Debido a la influencia de la luz del sol el agua en los océanos y  los lagos se calentará. Como resultado de esto se evaporará y será  transportada de nuevo a la atmósfera. Allí formará las nubes que con el  tiempo causarán la precipitación devolviendo el agua otra vez a la  tierra.<br />
La evaporación de los océanos es la clase más importante de evaporación.</p>
<p><span>6) Condensación</span><br />
En contacto con la atmósfera el vapor de agua se transformará de nuevo  a líquido, de modo que sea visible en el aire. Estas acumulaciones de  agua en el aire son lo que llamamos las nubes.</p>
</div>

@endsection