<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('', 'InicioController@mostrarInicio');

Route::get('inicio', 'InicioController@mostrarInicio');



Route::get('quienes-somos', 'EmpresaController@mostrarQuienesSomos');

Route::get('resena-historica', 'EmpresaController@mostrarResenaHistorica');

Route::get('datos-de-empresa', 'EmpresaController@mostrarDatosDeEmpresa');

Route::get('departamentos', 'EmpresaController@mostrarDepartamentos');

Route::get('plantas-de-agua-potable', 'EmpresaController@mostrarPlantasDeAguaPotable');

Route::get('planta-dep-aguas-servidas', 'EmpresaController@mostrarPlantaDepAguasServidas');

Route::get('distribucion', 'EmpresaController@mostrarDistribucion');

Route::get('laboratorios', 'EmpresaController@mostrarLaboratorios');

Route::get('politicas-de-calidad', 'EmpresaController@mostrarPoliticasDeCalidad');

Route::get('tecnologias', 'EmpresaController@mostrarTecnologias');

Route::get('inversiones', 'EmpresaController@mostrarInversiones');

Route::get('memorias-y-eeff', 'EmpresaController@mostrarMemoriasYEEFF');



Route::get('tarifas', 'ServicioClienteController@mostrarTarifas');

Route::get('verificar-documento-electronico', 'ServicioClienteController@mostrarVerificarDocumentoElectronico');

Route::get('reglamentacion-y-compromiso', 'ServicioClienteController@mostrarReglamentacionYCompromiso');

Route::get('centro-de-atencion-al-cliente', 'ServicioClienteController@mostrarCentroDeAtencionAlCliente');

Route::get('centro-de-pagos', 'ServicioClienteController@mostrarCentroDePagos');

Route::get('subsidio', 'ServicioClienteController@mostrarSubsidio');

Route::get('contratistas-autorizados', 'ServicioClienteController@mostrarContratistasAutorizados');

Route::get('preguntas-frecuentes', 'ServicioClienteController@mostrarPreguntasFrecuentes');

Route::get('conozca-su-boleta', 'ServicioClienteController@mostrarConozcaSuBoleta');

Route::get('proveedores', 'ServicioClienteController@mostrarProveedores');

Route::get('solicitudes-de-factibilidad', 'ServicioClienteController@mostrarSolicitudesDeFactibilidad');


Route::get('cortes-programados', 'ArticulosController@mostrarCortesProgramados');

Route::get('cortes-de-emergencia', 'ArticulosController@mostrarCortesDeEmergencia');

Route::get('noticias', 'ArticulosController@mostrarNoticias');

Route::get('tareas-escolares', 'ArticulosController@mostrarTareasEscolares');

Route::get('consejos-para-ahorrar-agua', 'ArticulosController@mostrarConsejosParaAhorrarAgua');


Route::get('contacto', 'ContactoController@mostrarContacto');

Route::post('contacto', 'ContactoController@ingresarReclamo');