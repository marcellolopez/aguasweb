<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

class ServicioClienteController extends Controller
{
    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return Response
     */
    public function mostrarTarifas()
    {
        return view('servicio_cliente/tarifas');
    }

    public function mostrarVerificarDocumentoElectronico()
    {
        return view('servicio_cliente/verificar-documento-electronico');
    }

    public function mostrarReglamentacionYCompromiso()
    {
        return view('servicio_cliente/reglamentacion-y-compromiso');
    }

    public function mostrarCentroDeAtencionAlCliente()
    {
        return view('servicio_cliente/centro-de-atencion-al-cliente');
    }

    public function mostrarCentroDePagos()
    {
        return view('servicio_cliente/centro-de-pagos');
    }

    public function mostrarSubsidio()
    {
        return view('servicio_cliente/subsidio');

    }

    public function mostrarContratistasAutorizados()
    {
        return view('servicio_cliente/contratistas-autorizados');
    }

    public function mostrarPreguntasFrecuentes()
    {
        return view('servicio_cliente/preguntas-frecuentes');
    }

    public function mostrarConozcaSuBoleta()
    {
        return view('servicio_cliente/conozca-su-boleta');
    }

    public function mostrarProveedores()
    {
        return view('servicio_cliente/proveedores');
    }

    public function mostrarSolicitudesDeFactibilidad()
    {
        return view('servicio_cliente/solicitudes-de-factibilidad');
    }
    
}