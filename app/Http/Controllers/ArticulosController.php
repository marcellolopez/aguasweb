<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

class ArticulosController extends Controller
{
    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return Response
     */
    public function mostrarCortesProgramados()
    {
        return view('articulos/cortes-programados');
    }

    public function mostrarCortesDeEmergencia()
    {
        return view('articulos/cortes-de-emergencia');
    }

    public function mostrarNoticias()
    {
        return view('articulos/noticias');
    }

    public function mostrarTareasEscolares()
    {
        return view('articulos/tareas-escolares');
    }

    public function mostrarConsejosParaAhorrarAgua()
    {
        return view('articulos/consejos-para-ahorrar-agua');
    }
   
}