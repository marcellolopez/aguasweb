<?php

namespace App\Http\Controllers;
use DB;
use App\Http\Controllers\Controller;
use input;

class ContactoController extends Controller
{
    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return Response
     */


    public function  mostrarContacto()
  {    
    $motivos = DB::table('motivos')->get(); 


  /*  if (count($servicios)==0)
    {
     return View::make('contacto/contacto')->with('mensaje_login','<div id="div-alert" name="div-alert" class="alert alert-warning text-center"><br>Usted no tiene servicios asociados aun.<br>Puede agregar un servicio haciendo click <a href="agregar">aquí<a><div>');

   }
  */
   {
     //return View::make('contacto/contacto')->with(array('motivos' => $motivos ));
     return view('contacto/contacto', ['motivos' => $motivos]);
   }


 }

//funcion que almacena el reclamo en la bd
 public function  ingresarReclamo()
 {   
  /*
  $reclamos = new Reclamos;
  $reclamos->id_cliente = null;
  $reclamos->id_motivo =Input::get('ddMotivos');
  $reclamos->nombre =Input::get('txtNombre');
  $reclamos->apellido_paterno =Input::get('txtApellidoPaterno');  
  $reclamos->apellido_Materno =Input::get('txtApellidoMaterno');   
  $reclamos->rut =Input::get('txtRut');
  $reclamos->numero_servicio = Input::get('txtNumero_servicio');    
  $reclamos->direccion = Input::get('txtDireccion');      
  $reclamos->email = Input::get('txtEmail');  
  $reclamos->telefono = Input::get('txtTelefono');  
  $reclamos->mensaje = Input::get('cmbMensaje');

  $reclamos->save();
  */

  DB::insert('insert into reclamo (
            id_cliente, 
            id_motivo,
            nombre,
            apellido_paterno,
            apellido_materno,
            rut,
            numero_servicio,
            direccion,
            email,
            telefono,
            mensaje
            ) 
            values (?,?,?,?,?,?,?,?,?,?,?)',
            [
            null,
            Input::get('ddMotivos'),
            Input::get('txtNombre'),
            Input::get('txtApellidoPaterno'),  
            Input::get('txtApellidoMaterno'),   
            Input::get('txtRut'),
            Input::get('txtNumero_servicio'),    
            Input::get('txtDireccion'),      
            Input::get('txtEmail'),  
            Input::get('txtTelefono'),  
            Input::get('cmbMensaje')
            ]
            );

  

  $motivos = DB::table('motivos')->get(); 


  
  $motivo = DB::table('motivos')->where('id','=',Input::get('ddMotivos'))->first(); 

  Mail::send('aviso_reclamo',array(
    'reclamo' => $reclamos , 
    'motivo' => $motivo, 
    'cliente' => $cliente , 
    'servicio' => $servicios , 
    'username' => ucfirst(Auth::user()['nombre'])), 
  function($message)
  {
    $message->to(Input::get('txtEmail'),
      ucfirst(Input::get('txtNombre')))
    ->cc('info@aguasdecima.cl')
    ->subject('Ingreso de reclamo');
  }
  );
  Mail::send('aviso_reclamo',array(
    'reclamo' => $reclamos , 
    'motivo' => $motivo, 
    'cliente' => $cliente , 
    'servicio' => $servicios , 
    'username' => ucfirst(Auth::user()['nombre'])), 
  function($message)
  {
    $message->to(Auth::user()['email'],
      ucfirst(Auth::user()['nombre']))
    ->cc('info@aguasdecima.cl')
    ->subject('Ingreso de reclamo');
  }
  );
  $motivo = DB::table('motivos')->where('id','=',Input::get('ddMotivos'))->get(); 



  return View::make('contacto/contacto')->with(array('mensaje' => '<div disabled="true" id="div-alert" name="div-alert" class="alert alert-success text-center">El reclamo se ingreso exitosamente.<br>Se ha enviado una copia de este reclamo a tu email.</div>' , 'motivos' => $motivos , 'servicios' => $servicios));

  }
}