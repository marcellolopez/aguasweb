<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

class EmpresaController extends Controller
{
    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return Response
     */

    public function mostrarTarifas()
    {
        return view('empresa/tarifas');
    }

    public function mostrarVerificarDocumentoElectronico()
    {
        return view('empresa/verificar-documento-electronico');
    }    

    public function mostrarQuienesSomos()
    {
        return view('empresa/quienes-somos');
    }

    public function mostrarResenaHistorica()
    {
        return view('empresa/resena-historica');
    }

    public function mostrarDatosDeEmpresa()
    {
        return view('empresa/datos-de-empresa');
    }

    public function mostrarDepartamentos()
    {
        return view('empresa/departamentos');
    }

    public function mostrarPlantasDeAguaPotable()
    {
        return view('empresa/plantas-de-agua-potable');
    }

    public function mostrarPlantaDepAguasServidas()
    {
        return view('empresa/planta-dep-aguas-servidas');
    }

    public function mostrarDistribucion()
    {
        return view('empresa/distribucion');
    }

    public function mostrarLaboratorios()
    {
        return view('empresa/laboratorios');
    }

    public function mostrarPoliticasDeCalidad()
    {
        return view('empresa/politicas-de-calidad');
    }

    public function mostrarTecnologias()
    {
        return view('empresa/tecnologias');
    }
    
    public function mostrarInversiones()
    {
        return view('empresa/inversiones');
    }

    public function mostrarMemoriasYEEFF()
    {
        return view('empresa/memorias-y-eeff');
    }
}